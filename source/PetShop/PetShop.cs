using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public class PetShop
    {
        private readonly IList<Pet> _petsInTheStore;

        public PetShop(IList<Pet> petsInTheStore)
        {
            this._petsInTheStore = petsInTheStore;
        }

        public IEnumerable<Pet> AllPets()
        {
            return new ReadOnlySet<Pet>( _petsInTheStore);
        }

        public void Add(Pet newPet)
        {
            if (this._petsInTheStore.Contains(newPet))
                return;


            this._petsInTheStore.Add(newPet);
        }

        public IEnumerable<Pet> AllPetsSortedByName()
        {
            List<Pet> copyOfPets = new List<Pet>(_petsInTheStore);
            copyOfPets.Sort((pet1, pet2) => String.Compare(pet1.name, pet2.name, StringComparison.Ordinal));
            return copyOfPets;
        }

        public IEnumerable<Pet> AllPetsButNotMice()
        {
            return _petsInTheStore.ThatSatisfy(new Not<Pet>(Pet.IsMouse()));
        }

        public IEnumerable<Pet> AllDogsBornAfter2010()
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsAnyOfRace(Race.Dog).And(Pet.IsBornAfter(2010)));
        }

        public IEnumerable<Pet> AllMaleDogs()
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsMale().And(Pet.IsAnyOfRace(Race.Dog)));
        }

        public IEnumerable<Pet> AllPetsBornAfter2011OrRabbits()
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsBornAfter(2011).Or(Pet.IsAnyOfRace(Race.Rabbit)));
        }
    }
}
