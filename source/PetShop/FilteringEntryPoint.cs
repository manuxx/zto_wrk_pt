﻿using System;
using Training.DomainClasses;

namespace Training.Spec
{
    public class FilteringEntryPoint<TItem, TProperty> 
    {
        public readonly Func<TItem, TProperty> _propertySelector;
        private readonly bool not;
        private readonly Criteria<TItem> leftAndCriteria;

        public FilteringEntryPoint(Func<TItem, TProperty> propertySelector) : this(propertySelector,false)
        {
        }

        private FilteringEntryPoint(Func<TItem, TProperty> propertySelector, bool newNot)
        {
            _propertySelector = propertySelector;
            not = newNot;
        }

        public FilteringEntryPoint(Func<TItem, TProperty> propertySelector, Criteria<TItem> newleftAndCriteria)
        {
            _propertySelector = propertySelector;
            leftAndCriteria = newleftAndCriteria;
        }

        public FilteringEntryPoint<TItem, TProperty> Not()
        {
            return new FilteringEntryPoint<TItem, TProperty>(_propertySelector, ! not);
        }

        public Criteria<TItem> ApplyContextForResult(Criteria<TItem> resultCriteria)
        {
            if (not)
            {
                resultCriteria = new Not<TItem>(resultCriteria);
            }
            if (leftAndCriteria != null)
                resultCriteria = new AndCriteria<TItem>(leftAndCriteria, resultCriteria);
            return resultCriteria;
        }
    }
}