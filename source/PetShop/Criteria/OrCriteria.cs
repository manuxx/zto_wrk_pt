namespace Training.DomainClasses
{
    public class OrCriteria<TItem> : BinaryCriteria<TItem>
    {

        public OrCriteria(Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria) : base(leftCriteria, rightCriteria)
        {
        }
        

        public override bool IsSatisfiedBy(TItem item)
        {
            return _leftCriteria.IsSatisfiedBy(item) || _rightCriteria.IsSatisfiedBy(item);
        }
    }
}