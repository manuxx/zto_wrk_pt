﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Training.Spec;


    public class AndClass<TItem>
    {
        private Criteria<TItem> leftCriteria;

        public AndClass(Criteria<TItem> leftCriteria)
        {
            this.leftCriteria = leftCriteria;
        }

        public FilteringEntryPoint<TItem, TProperty> hasAn<TProperty>(Func<TItem, TProperty> propertySelector)
        {
            return new FilteringEntryPoint<TItem, TProperty>(propertySelector, leftCriteria);
        }
    }

