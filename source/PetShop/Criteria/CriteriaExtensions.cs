using Training.DomainClasses;

public static class CriteriaExtensions
{
    public static AndCriteria<TItem> And<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria)
    {
        return new AndCriteria<TItem>(leftCriteria,rightCriteria);
    }

    public static AndClass<TItem> And<TItem>(this Criteria<TItem> leftCriteria)
    {
        return new AndClass<TItem>(leftCriteria);
    }

    public static OrCriteria<TItem> Or<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightcriteria)
    {
        return new OrCriteria<TItem>(leftCriteria,rightcriteria);
    }
}