namespace Training.DomainClasses
{
    public class Not<TItem> : Criteria<TItem>
    {
        private readonly Criteria<TItem> _criteriaToNegate;

        public Not(Criteria<TItem> criteriaToNegate)
        {
            _criteriaToNegate = criteriaToNegate;
        }

        public bool IsSatisfiedBy(TItem item)
        {
            return !_criteriaToNegate.IsSatisfiedBy(item);
        }
    }
}