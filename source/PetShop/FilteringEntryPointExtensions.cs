using System;
using System.Collections.Generic;
using Training.DomainClasses;
using Training.Spec;

public static class FilteringEntryPointExtensions
{
    public static Criteria<TItem> EqualsTo<TItem, TProperty>(this FilteringEntryPoint<TItem, TProperty> filteringEntryPoint, TProperty proprtyValue)
    {
        Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(item => filteringEntryPoint._propertySelector(item).Equals(proprtyValue));
        return filteringEntryPoint.ApplyContextForResult(resultCriteria);
    }

    public static Criteria<TItem> GreaterThan<TItem, TProperty>(this FilteringEntryPoint<TItem, TProperty> filteringEntryPoint, TProperty propertyValue) where TProperty:IComparable<TProperty>
    {
        Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(item => filteringEntryPoint._propertySelector(item).CompareTo(propertyValue) > 0);
        return filteringEntryPoint.ApplyContextForResult(resultCriteria);
    }

    public static Criteria<TItem> EqualToAnyOf<TItem, TProperty>(this FilteringEntryPoint<TItem, TProperty> filteringEntryPoint, params TProperty[] paramValues)
    {
        var allowedValues = new List<TProperty>(paramValues);
        Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(item => allowedValues.Contains(filteringEntryPoint._propertySelector(item)));
        return filteringEntryPoint.ApplyContextForResult(resultCriteria);

    }
}