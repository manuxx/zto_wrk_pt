using System;

namespace Training.Spec
{
    public class Where<TItem>
    {
        public static FilteringEntryPoint<TItem, TProperty> hasAn<TProperty>(Func<TItem, TProperty> propertySelector) 
        {
            return new FilteringEntryPoint<TItem, TProperty>(propertySelector);
        }

    }
}